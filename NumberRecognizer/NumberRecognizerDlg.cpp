﻿#include "stdafx.h"
#include "NumberRecognizer.h"
#include "NumberRecognizerDlg.h"
#include "afxdialogex.h"
#include <time.h>
#include <direct.h>
#include <vector>
#include <algorithm>
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace tesseract;
using namespace Classifier;
using std::min;

Pix* NumberRecognizerDlg::matToPix(Mat *mat)
{
	Pix *pixd = pixCreate(mat->size().width, mat->size().height, 8);
	for (int y = 0; y<mat->rows; y++) {
		for (int x = 0; x<mat->cols; x++) {
			pixSetPixel(pixd, x, y, (l_uint32)mat->at<uchar>(y, x));
		}
	}
    
	pixSetResolution(pixd, 300, 300);
	return pixd;
}

NumberRecognizerDlg::NumberRecognizerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(NumberRecognizerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_ptrArray1 = NULL;
	m_nIndex = 0;
}

void NumberRecognizerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(NumberRecognizerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON5, &NumberRecognizerDlg::OnBnClickedButtonExit)
    ON_BN_CLICKED(IDC_BUTTON2, &NumberRecognizerDlg::OnBnClickedButtonFindWithContour)
    ON_BN_CLICKED(IDC_BUTTON1, &NumberRecognizerDlg::OnBnClickedButtonFindWithTess)
    ON_BN_CLICKED(IDC_BUTTON3, &NumberRecognizerDlg::OnBnClickedButtonRecognizeWithSVM)
    ON_BN_CLICKED(IDC_BUTTON4, &NumberRecognizerDlg::OnBnClickedButtonTrainingSVM)
    ON_BN_CLICKED(IDC_BUTTON6, &NumberRecognizerDlg::OnBnClickedButtonScaleData)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


void NumberRecognizerDlg::OnBnClickedButtonExit()
{
	// TODO: Add your control notification handler code here
	exit(0);
}

void NumberRecognizerDlg::OnBnClickedButtonFindWithTess()
{
    imgFilePath = OpenImage();
    if (imgFilePath.compare("") == 0)
        return;

    InitTesseract();
    ShowImageWithTess(imgFilePath);
}

void NumberRecognizerDlg::OnBnClickedButtonFindWithContour()
{
    imgFilePath = OpenImage(); 
    if (imgFilePath.compare("")==0)
        return;
    ShowImageWithContour(imgFilePath);
}

void NumberRecognizerDlg::InitTesseract()
{
	string tessDir = GetCurrentDir();
	tessDir.append("\\Bin64");

	tessApi = new tesseract::TessBaseAPI();
	if (tessApi->Init(tessDir.c_str(), "kor")) {
		fprintf(stderr, "Could not initialize tesseract.\n");
		exit(1);
	}

	tessApi->SetVariable("language_model_penalty_non_dict_word", "1.0f");
	tessApi->SetVariable("language_model_penalty_non_freq_dict_word", "1.0f");
}

void NumberRecognizerDlg::OnBnClickedButtonRecognizeWithSVM()
{
    const int w = 24, h = 24;

    string base = GetCurrentDir() + "\\training";
    string saveFile = base + "\\Savedata\\model";
    CLibSVMWrapper svm;
    svm.SetDimensionCount(w*h);
    svm.Load(saveFile.c_str());

    string filename = OpenImage();
    if (filename.compare("") == 0)
        return;
    Mat mat = imread(filename.c_str());
    resize(mat, mat, Size(w, h));

    double img[w * h];
    for (int i = 0; i < w * h; i++)
    {
        img[i] = mat.data[i]>128 ? 1:0;

        //img[i] = mat.data[i];
    }

    CLibSVMWrapper::_Node data;
    data.m_pValues = img;
    svm.Predict(data);
    printf("%s :: %d\n",filename.c_str(),data.m_nLabel); 
}

/// SVM Training
void NumberRecognizerDlg::OnBnClickedButtonTrainingSVM()
{
    string base = GetCurrentDir()+ "\\training";
    string pos = base+ "\\Pos\\*.*", neg = base+ "\\neg\\*.*";
    /*
    Ptr<SVM> svm = SVM::create();
    svm->setType(SVM::C_SVC);
    svm->setKernel(SVM::LINEAR);
    svm->setTermCriteria(TermCriteria(TermCriteria::EPS,100, 1e-6));
    */
    vector<vector<double> > posTrainData;
    vector<vector<double> > negTrainData;

    const int w = 24;
    const int h = 24;

    StoreDataOnDir(posTrainData,w,h, pos);
    StoreDataOnDir(negTrainData,w,h,neg);

    vector<vector<double> > finalData;
    vector<int> finalLabel;
    int i=0, j=0;

    while ( i<posTrainData.size() || j<negTrainData.size())
    { // 데이터 섞어주기(필요한가?)

        if (i < posTrainData.size())
        {
            finalData.push_back(posTrainData[i++]);
            finalLabel.push_back(1);
        }
        if (j < negTrainData.size())
        {
            finalData.push_back(negTrainData[j++]);
            finalLabel.push_back(0);
        }
    }
    CreateDirectory((base + "\\Savedata").c_str(), NULL);
    string saveFile = base + "\\Savedata\\model";
    /*
    Ptr<TrainData> tData = TrainData::create(finalData, ml::SampleTypes::ROW_SAMPLE, finalLabel);
    
    svm->train(tData);

    svm->save("saveFile");
    */
     
    double dData[w*h];
    CLibSVMWrapper svm;
    CLibSVMWrapper::_Node node;
    svm.SetDimensionCount(w*h);
    for (int k = 0; k < finalData.size(); k++)
    {
        for (int l = 0; l < finalData[k].size(); l++)
            dData[l] = finalData[k][l];
        node.m_nLabel = finalLabel[k];
        node.m_pValues = dData;
        svm.AddNode(node);
    }

    svm.Train();
    svm.Save(saveFile.c_str());
    printf("svmTrainDone\n");
}

/// training데이터를 변형해서 늘려주기위함
void NumberRecognizerDlg::OnBnClickedButtonScaleData()
{
    
}
/// 지정한 Dir에서 image파일들을 읽어와 width x height 데이터로 변환한다. 
void NumberRecognizerDlg::StoreDataOnDir(vector<vector<double> >& store,const int width,const int height, string path)
{
    //검색 클래스
    CFileFind finder;
    BOOL working = finder.FindFile(path.c_str());
    while (working)
    {
        if (finder.IsArchived())
        {
            string imagePath = finder.GetFilePath();
            Mat img = imread(imagePath);

            if (img.empty())
            {
                working = finder.FindNextFile();
                continue;
            }
            ///Training데이터 늘려주기
            
            Mat tmp = img.clone();
            //GaussianBlur(tmp, tmp, Size(3, 3), 0);
            
            //PutMatInStore(tmp, width, height, store);
            PutMatInStore(img,width,height,store);
        }
        working = finder.FindNextFile();
    }
}

void NumberRecognizerDlg::PutMatInStore(Mat& img,int width,int height, vector<vector<double> >& store)
{
    vector<double> row;
    resize(img, img, Size(width , height), 0 , 0);

    for (int i = 0; i < width*height; i++)
    {
        float tmp = img.data[i]>128 ?1:0;
        row.push_back(tmp);
    }
    store.push_back(row);
}

string NumberRecognizerDlg::GetCurrentDir()
{
    char curDir[1000];
    _getcwd(curDir, 1000);
    string base = curDir;
    //printf("%s", curDir);
    return base;
}

string NumberRecognizerDlg::OpenImage()
{
    // TODO: Add your control notification handler code here
    UpdateData(TRUE);
    string imgFileName = "";
    string dirPath;
    // File dialog
    char szFilter[] = "All Images( *.jpg, *.png, *.tif, *.* ) | *.jpg; *.JPG; *.png; *.PNG; *.tif; *.TIF;*.* || ";
    CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);
    if (IDOK == dlg.DoModal()) {
        imgFileName = dlg.GetPathName();
        dirPath = dlg.GetFolderPath();
    }
    else
        return "";


    // File Validation
    dirPath += "\\*.*";
    bool opend = currentFind.FindFile(dirPath.c_str());
    
    while (opend&& imgFileName.compare(currentFind.GetFilePath())!=0 )
    {
        opend = currentFind.FindNextFile();
        if (!opend)
        {
            AfxMessageBox("파일을 로드할 수 없음.");
            exit(0);
        }
        continue;
    }

    outDirName = GetCurrentDir();
    outDirName.append("\\training");

    return imgFileName;
}


void NumberRecognizerDlg::ReadImageToGray(string imgFileName) // after this, use grayImage 
{
    ////////////// setting image and window
    srcImage = imread(imgFileName.c_str());

    outImage = srcImage.clone();

    ///////////// image processing for tesseract. srcImage -> grayImage.
    Mat tmpImage;
    tmpImage = srcImage.clone();

    cvtColor(tmpImage, grayImage, CV_BGR2GRAY);
    //blur(grayImage, grayImage, cv::Size(2, 2));
    //////////// image processing for line(long line) detect and remove
    //Mat canny;
    //Canny(grayImage, canny, 150, 300);
    //blur(canny, canny, cv::Size(3, 3));
    
}

void NumberRecognizerDlg::LongLineRemove(Mat& gray)
{
    Mat canny;
    Canny(gray, canny, 150, 300);
    blur(canny, canny, cv::Size(3, 3));

    vector<Vec4i> lines;
    HoughLinesP(canny, lines, 1, CV_PI / 180, 80, gray.rows / 4, 5);
    for (size_t i = 0; i < lines.size(); i++)
    {
        line(gray, Point(lines[i][0], lines[i][1]),
            Point(lines[i][2], lines[i][3]), Scalar(255, 255, 255), 3);
    }
    
}

void NumberRecognizerDlg::SetWindowName(string wname)
{
    windowName = wname;
    namedWindow(wname, CV_WINDOW_NORMAL);
}

void NumberRecognizerDlg::ShowImageWithContour(string imgFileName)
{
    InitTesseract();

    selectMode = true;

    numBoxes = boxaCreate(0);
    negBoxes = boxaCreate(0);

    SetWindowName("Contour");
    if (m_ptrArray1 != NULL)
        m_ptrArray1->RemoveAll();
    m_ptrArray1 = new CPtrArray;

    ReadImageToGray(imgFileName); 

    adaptiveThreshold(grayImage, grayImage, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 7, 15);

    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    LongLineRemove(grayImage);

    Mat kernel = Mat::ones(3, 1, CV_8U);
    erode(grayImage, grayImage, kernel, Point(-1, -1), 2);

    Mat cannyImage;
    Canny(grayImage, cannyImage, 100, 150, 3);

    findContours(grayImage, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));

    for (size_t i = 0; i < contours.size(); i++)
    {
        Scalar color = Scalar(0, 0, 255);

        Mat cMat = Mat(contours[i]);
        
        Rect R = boundingRect(cMat);
        
        const int minWidth = 9;
        const int minHeight = 9;

        if (R.width < minWidth && R.height < minHeight)// 너무 작은게 많이잡혀서 일단 이렇게..
            continue;

        Box* box = boxCreate(R.x, R.y, R.width, R.height);

        tessApi->SetPageSegMode(PSM_SINGLE_CHAR);
        tessApi->SetImage(matToPix(&cMat));
        //tessApi->SetImage( cMat.data,cMat.cols,cMat.rows,1,1*cMat.cols );
        string s = tessApi->GetUTF8Text();
        bool isNum = false;
        for (int c = 0; c < s.length(); c++)
        {
            if (s[c] >= '0' && s[c] <= '9')
            {
                boxaAddBox(numBoxes, box, L_CLONE);
                isNum = true;
                break;
            }
        }
        if (!isNum)
            boxaAddBox(negBoxes, box, L_CLONE);
            
        //boxaAddBox(numBoxes, box, L_CLONE);
    }
    
    ShowRectWithBox(negBoxes, srcImage.clone(), windowName);
    //drawContours(srcImage, contours, -1, (0, 255, 0), 1);
    //imshow(windowName, grayImage.clone());
    //string str = format("Output.bmp");
    //imwrite( str.c_str(), matTemp );

    isLeftDown = false;
    //setMouseCallback(windowName, onMouse, this);

    // program acting. get key and interact. 
    // update image
    while (getWindowProperty(windowName, 0) >= 0)
    {
        int c = cvWaitKey();
        switch (c)
        {
        case SHOW_ALL_BOX:
            //ShowRectWithBox(allBoxes, srcImage.clone(), windowName);
            break;
        case SHOW_NUMBER_BOX:
            //ShowRectWithBox(numBoxes, srcImage.clone(), windowName);
            break;
        case SHOW_SRCIMAGE:
            imshow(windowName, srcImage);
            break;
        case SHOW_GRAYIMAGE:
            imshow(windowName, grayImage);
            break;
        }

    }

}

void NumberRecognizerDlg::ShowImageWithTess(string imgFileName) // work like main function
{
    selectMode = true;

    numBoxes = boxaCreate(0);
    negBoxes = boxaCreate(0);

    SetWindowName("Show");
    ReadImageToGray(imgFileName); 
    adaptiveThreshold(grayImage, grayImage, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 7, 15);

    LongLineRemove(grayImage);
	////////////setting tesseract
	Pix *image = matToPix(&grayImage);
    tessApi->SetPageSegMode(PSM_AUTO);
	tessApi->SetImage(image);
	///////////use tesseract
	Boxa* ab = tessApi->GetComponentImages(RIL_SYMBOL, true, NULL, NULL);

	for (int i = 0; i < ab->n; i++) {
		BOX* box = boxaGetBox(ab, i, L_CLONE);
		//if (box->w > box->h)continue;
		tessApi->SetRectangle(box->x, box->y, box->w, box->h);
		tessApi->SetPageSegMode(PSM_SINGLE_CHAR);
		string s = tessApi->GetUTF8Text();
        bool isNum = false;
		for (int c = 0; c < s.length(); c++)
		{
			if (s[c] >= '0' && s[c] <= '9')
			{
                boxaAddBox(numBoxes, box, L_CLONE);
                isNum = true;
				break;
			}
		}
        if(!isNum)
            boxaAddBox(negBoxes, box, L_CLONE);
	}
    ShowRectWithBox(numBoxes, srcImage.clone(), windowName);
	
	isLeftDown = false;
	setMouseCallback(windowName, onMouse, this);

	// program acting. get key and interact. 
	// update image
	while (getWindowProperty(windowName, 0) >= 0)
	{
		int c = cvWaitKey();
		switch (c)
		{
        case CHANGE_SELECT_MODE:
            selectMode = !selectMode;
        break;
		case SHOW_ALL_BOX:
        {
            Boxa * allbox = boxaCopy(numBoxes, L_COPY);
            boxaJoin(allbox, negBoxes, 0, -1);
            ShowRectWithBox(allbox, srcImage.clone(), windowName);
        }
        break;
		case SHOW_NUMBER_BOX:
			ShowRectWithBox(numBoxes,srcImage.clone(), windowName);
			break;
		case SHOW_SRCIMAGE:
			imshow(windowName, srcImage);
			break;
		case SHOW_GRAYIMAGE:
			imshow(windowName, grayImage);
			break;
		case EXTRACT_NUM: // extract images checked in numBoxes 
            string posDir = outDirName+ "\\Pos\\";
            ExtractBoxesInDir(posDir, numBoxes);
            string negDir = outDirName + "\\Neg\\";
            ExtractBoxesInDir(negDir, negBoxes);

            //// to the next file
            imgFilePath = GetNextFile(imgFilePath);

            if (imgFilePath.compare("") !=0 )
            {
                ReleaseBoxa(numBoxes);
                ReleaseBoxa(negBoxes);
                ShowImageWithTess(imgFilePath);
            }
            else // end of directory 
            {
                AfxMessageBox("End of Directory");
                exit(0);
            }

			break;
		}

	}
}

// directory로 box들의 이미지 추출
void NumberRecognizerDlg::ExtractBoxesInDir(string dirpath,Boxa* boxes)
{
    int last = findNumberOfFilesInDirectory(dirpath);
    for (int i = 0; i < boxes->n; i++)
    {
        Box* b = boxaGetBox(boxes, i, L_CLONE);
        Rect r(b->x, b->y, b->w, b->h);

        string extFileName = (dirpath + std::to_string(i + last + 1) + ".jpg");

        Mat extImage = srcImage(r);
        //CreateDirectory(outDirName.c_str(), NULL);
        imwrite(extFileName.c_str(), extImage);
    }

}

/// OpenImage한 폴더에서 다음 파일을 받아옴 
/// 파일 읽어오기 실패시 ""
string NumberRecognizerDlg::GetNextFile(string filepath)
{
    printf("%s\n", currentFind.GetFilePath());

    bool opend = currentFind.FindNextFile();

    if (!opend)
        return "";

    if( currentFind.IsArchived() )
        return currentFind.GetFilePath();
    return GetNextFile(filepath);
}

bool NumberRecognizerDlg::SearchAreaInBoxa(BOX& area, Boxa* in, Boxa* out, bool isAllbox)
{
    bool isIn = false;
    for (int i = 0; i < out->n; i++)
    {
        Box* b = boxaGetBox(out, i, L_CLONE);

        isIn = isInBox(area, *b);
        if (isIn)
        {
            boxaRemoveBox(out, i);
            boxaAddBox(in, b, L_CLONE);
            i--; // for iterating
            if (!isAllbox)
                return true;
        }
    }
    return isIn;
}

void NumberRecognizerDlg::onMouse(int event, int x, int y, int flags, void* userdata)
{
	NumberRecognizerDlg* data = static_cast<NumberRecognizerDlg*>(userdata);
	// because opencv is programmed in c style, we should pass class variable through userdata
	
	if (event == EVENT_LBUTTONDOWN)
	{
		data->isLeftDown = true;
		data->ldx = x;
		data->ldy = y;
	}
    else if (event == EVENT_MOUSEMOVE)
    {
        if (data->isLeftDown)
        {
            int sx = data->ldx;
            int sy = data->ldy;

            if (!data->isLeftDown)return;

            Box* box;
            box = boxCreate(min(sx, x), min(sy, y), abs(sx - x), abs(sy - y));
            
        }
    }
	else if (event == EVENT_LBUTTONUP)
	{
		int sx = data->ldx;
		int sy = data->ldy;
		Box *box; 
		if (!data->isLeftDown)return;
		box = boxCreate(min(sx, x), min(sy, y), abs(sx - x), abs(sy - y));
        
        if (data->selectMode)
            data->SearchAreaInBoxa(*box, data->numBoxes, data->negBoxes,true);
        else
            data->SearchAreaInBoxa(*box, data->negBoxes, data->numBoxes,true);

		data->ShowRectWithBox(data->numBoxes, data->srcImage.clone(), "Show");
		data->isLeftDown = false;

        boxDestroy(&box);
	}
	else if (event == EVENT_RBUTTONDOWN)
	{
		bool isSearchNumBox = false;
		for (int i = 0; i < data->numBoxes->n; i++)
		{
            Box* b = boxaGetBox(data->numBoxes, i, L_CLONE);
			if (data->isInBox(*b, x, y))
			{
                boxaRemoveBox(data->numBoxes, i);
                boxaAddBox(data->negBoxes, b, L_CLONE);
				data->ShowRectWithBox(data->numBoxes, data->srcImage.clone(), "Show");
				isSearchNumBox = true;
				break;
			} 
		}
		if (isSearchNumBox)return;
		for (int i = 0; i < data->negBoxes->n; i++)
		{
            Box* b = boxaGetBox(data->negBoxes, i, L_CLONE);
			if (data->isInBox(*b, x, y))
			{
                boxaRemoveBox(data->negBoxes, i);
                boxaAddBox(data->numBoxes, b, L_CLONE);
				data->ShowRectWithBox(data->numBoxes, data->srcImage.clone(), "Show");
				break;
			}
		}

	}
}

Mat& NumberRecognizerDlg::ShowRectWithBox(Boxa * boxes,  Mat& img,string windowname)
{
	for (int i = 0; i < boxes->n; i++) {
        Box* b = boxaGetBox(boxes,i,L_CLONE);
		cv::Rect d(b->x, b->y, b->w, b->h);
		const cv::Scalar color = Scalar(0, 0, 255);
		rectangle(img, d, color);
	}
	imshow(windowname.c_str(), img);
	return img;
}


bool NumberRecognizerDlg::isInBox(Box &box, int x, int y)
{
	if (x > box.x && y > box.y && x < box.x + box.w && y < box.y + box.h)
		return true;
	return false;
}

bool NumberRecognizerDlg::isInBox(Box &boxArea, Box &boxPoint)
{
    int x = boxPoint.x + boxPoint.w / 2; //boxPoint의 중점좌표
    int y = boxPoint.y + boxPoint.h / 2;

    return isInBox(boxArea, x, y);
}

char * NumberRecognizerDlg::RecognizeCharacterWithRect(Rect* r)
{
	tessApi->SetRectangle(r->x,r->y,r->width,r->height);
	tessApi->SetPageSegMode(tesseract::PageSegMode::PSM_SINGLE_CHAR);
	return tessApi->GetUTF8Text();
}

void NumberRecognizerDlg::ReleaseBoxa(BOXA* boxa)
{
    if (boxa == NULL)
        return;
    for (int i = 0; i < boxa->n; i++)
    {
        Box *b = boxaGetBox(boxa,i, L_CLONE);
        if(b!=NULL)
            boxDestroy(&b);
    }
    boxaDestroy(&boxa);
}

void NumberRecognizerDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: Add your message handler code here
	if ( m_ptrArray1 != NULL )
		m_ptrArray1->RemoveAll( );

    ReleaseBoxa(numBoxes);
    ReleaseBoxa(negBoxes);
}

int NumberRecognizerDlg::findNumberOfFilesInDirectory(std::string& path)
{
	int counter = 0;
	WIN32_FIND_DATA FindData;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	string dest = path;
	dest += "*";

	// Start iterating over the files in the path directory.
	hFind = FindFirstFile(dest.c_str(), &FindData);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do 
		{
			counter++;
		} while (FindNextFile(hFind, &FindData));
		FindClose(hFind);
	}
	else {
		printf("Failed to find path: %s", dest.c_str());
	}
	return counter;
}

// NumberRecognizerDlg 메시지 처리기

BOOL NumberRecognizerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	//SetConsoleOutputCP(65001);

	std::cout << "Init" << std::endl;

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

									// TODO: 여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void NumberRecognizerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR NumberRecognizerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
