
// NumberRecognizerDlg.h : 헤더 파일
//

#pragma once

#include "allheaders.h"
#include "baseapi.h"
#include "svm\LibSVMWrapper.h"
#include <string>
#include <filesystem>

using namespace tesseract;
using namespace cv;
using namespace cv::ml;
using std::string;
using std::vector;
namespace fs = std::experimental::filesystem;

const char SHOW_NUMBER_BOX = 'n';
const char SHOW_ALL_BOX = 'a';
const char SHOW_SRCIMAGE = 's'; 
const char CHANGE_SELECT_MODE = 'e';
const char SHOW_GRAYIMAGE = ' '; 
const char EXTRACT_NUM = 13; // enter

// NumberRecognizerDlg 대화 상자
class NumberRecognizerDlg : public CDialogEx
{
// 생성입니다.
public:
	NumberRecognizerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

	Mat			matInput;
	Mat			m_Template0;
	Mat			m_Template1;
	Mat			m_Template2;

	Mat			m_TrainingData;

	CPtrArray*	m_ptrArray1;
	CPtrArray*	m_ptrArray2;

	int			m_nForward;
	int			m_nAfterOrientation;
	CPtrArray	m_ptLeftArray;
	CPtrArray	m_ptRightArray;
	CPtrArray	m_ptFirstArray;
	
	int			m_nIndex;



// 대화 상자 데이터입니다.
	enum { IDD = IDD_NUMBERRECOGNIZER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:

	int nameNumbering;
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	Mat srcImage;
	Mat grayImage;
	Mat outImage; 
	Pix *image;
	
	void InitTesseract();

    string GetNextFile(string filepath);
    string OpenImage();
    void SetWindowName(string wname);

    void ReadImageToGray(string imgFileName);
    void ShowImageWithContour(string imgFileName);
    void ShowImageWithTess(string imgFileName);
    void LongLineRemove(Mat& gray);
    void StoreDataOnDir(vector<vector<double> >& store,const int width,const int height, string path);
    void ExtractBoxesInDir(string dirpath,Boxa* boxs);
    Mat& ShowRectWithBox(Boxa* boxs, Mat& img,string windowname);
    Pix* matToPix(Mat *mat);

    // box안에 x,y가 있는지 여부
	bool isInBox(Box &box, int x, int y);
    bool isInBox(Box &boxArea, Box &boxPoint);

	char * RecognizeCharacterWithRect(Rect* r);
    
    // out에 area영역에 맞는 box가 있는지 찾고 있으면 out에서 빼서 in에 넣는다. box전체를 검사할건지 하나 찾으면 나올건지 여부
    bool SearchAreaInBoxa(BOX& area, Boxa* in, Boxa* out, bool isAllbox); 
    void PutMatInStore(Mat& img, int width, int height, vector<vector<double> >& store);

    afx_msg void OnBnClickedButtonExit();
	afx_msg void OnBnClickedButtonFindWithTess();
    afx_msg void OnBnClickedButtonFindWithContour();
    afx_msg void OnBnClickedButtonRecognizeWithSVM();
    afx_msg void OnBnClickedButtonTrainingSVM();
    afx_msg void OnBnClickedButtonScaleData();
	afx_msg void OnDestroy();
	static void onMouse(int event, int x, int y, int flags, void* userdata);
   
    void ReleaseBoxa(BOXA* boxa);
    string GetCurrentDir();
	int findNumberOfFilesInDirectory(std::string& path);

private:
	tesseract::TessBaseAPI *tessApi;
	string outDirName;

    CFileFind currentFind;

    string imgFilePath;
    string windowName;
    bool isLeftDown;
    bool selectMode; // 왼쪽클릭 선택시 박스/해제 토글
    int ldx;
    int ldy;
    Boxa* numBoxes = NULL;
    Boxa* negBoxes = NULL;
};
