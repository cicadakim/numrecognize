#pragma once
/****************************************************************************************
* Include files
****************************************************************************************/
#include "stdafx.h"
const double PI = 3.14159278;

class ImageUtil {

public:
	IplImage* getRoiFrame( IplImage *frame, CvRect rect );
	IplImage* getRoiFrame( IplImage *frame, int minR, int minC, int maxR, int maxC, int radius );
	double angle( cv::Point pt1, cv::Point pt2, cv::Point pt0 );
	void drawDottedLine(IplImage* img,CvPoint pt1, CvPoint pt2,CvScalar color, int thickness = 1, int lenghOfDots =2, int lineType = 8, int leftToRight = 1);
	void drawDottedRect(IplImage* img,CvPoint pt1, CvPoint pt2,CvScalar color, int thickness = 1, int lenghOfDots = 2, int lineType = 8);
	void drawDottedRect(IplImage* img,CvRect rect,CvScalar color, int thickness = 1, int lenghOfDots = 2, int lineType = 8);
	int getAvgIntensity( IplImage *gray );
	void setRoi( IplImage *src, IplImage *dst, CvPoint pt );
};


