#pragma once
/****************************************************************************************
* Include files
****************************************************************************************/
#include "../stdafx.h"
#include "LibSVMWrapper.h"
#include "../UTIL/ImageUtil.h"
#include "../UTIL/TrainDataUtil.h"
#include "../UTIL/IoUtil.h"
#include "../common/MctConstant.h"
#include <io.h>
#include <fstream>
#include "../util/RsdVerifier.h"

using namespace std;
using namespace Classifier;

/****************************************************************************************
* Defines
****************************************************************************************/
#define _N_CLASS	2
#define _N_DIMS		256

/**
@class   SVMRecognizer
@SVM을 이용한 표지판 검증 및 인식을 수행하는 클래스
*/
class SVMRecognizer {
	/************************************************************************
	Member Variables
	************************************************************************/
private:
	MctGenerator		mctGen;					/// MCT Generator	
	ImageUtil			iUtil;					/// Image Utility
	TrainDataUtil		tUtil;					/// Train Data Utility
	IoUtil				ioUtil;
	RsdVerifier			rsd;

	int**				mctDualHist;
	int**				mctQuadHist;
	char				svmWeightFileName[10000];
	bool				isOnceInited;
protected:
	
	CLibSVMWrapper		svmWrapper[5];			/// 표지판 인식 모듈		
	

	/****************************************************************************************
	* Function prototypes
	****************************************************************************************/
public:
	/*
	SVMRecognizer();

	void loadWeights();	

	void trainLevelClassifier( char *baseDir, int levelCode );

	bool classifyRoi( IplImage *roi, int &classNo, double &prob );
	

protected:
	void trainClassifierLevel1( char *baseDir );
	void trainClassifierLevel2( char *baseDir );
	void trainClassifierLevel3Etc( char *baseDir );
	void trainClassifierLevel3SpeedLimit( char *baseDir );
	void trainClassifierLevel4SpeedLimit( char *baseDir );

	void classifyLevel1( IplImage *roiImg, int &classNo, double &prob );
	void classifyLevel2( IplImage *roiImg, int &classNo, double &prob );
	void classifyLevel3Etc( IplImage *roiImg, int &classNo, double &prob );
	void classifyLevel3SpeedLimit( IplImage *roiImg, int &classNo, double &prob );	

	void getVerticalByteMctHist( IplImage *gray, int **mctHist);	
	void getHorizontalByteMctHist( IplImage *gray, int **mctHist);
	void getQuadByteMctHist( IplImage *gray, int **mctHist );

	double* mctHistNormalization( int* hist );
	*/
};