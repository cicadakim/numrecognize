#pragma once
#include "Stdafx.h"
#include "SVMRecognizer.h"

/*

SVMRecognizer::SVMRecognizer()
{	
	isOnceInited = false;
	mctDualHist = (int**)malloc( sizeof(int*) * 2 );	
	for( int i = 0; i < 2; i++ ) {
		mctDualHist[i] = (int*)malloc( sizeof(int) * 256 );	
	}
	
	mctQuadHist = (int**)malloc( sizeof(int*) * 4 );
	for( int i = 0; i < 4; i++ ) {
		mctQuadHist[i] = (int*)malloc( sizeof(int) * 256 );	
	}
}

void SVMRecognizer::loadWeights()
{	
	if( isOnceInited == false ) {
		
		sprintf( svmWeightFileName, "%s/svm_weight_level10.csv", tUtil.dirPrefix );
		svmWrapper[0].SetDimensionCount(512);
		svmWrapper[0].Load( svmWeightFileName );
		sprintf( svmWeightFileName, "%s/svm_weight_level20.csv", tUtil.dirPrefix );
		svmWrapper[1].SetDimensionCount(512);
		svmWrapper[1].Load( svmWeightFileName );
		sprintf( svmWeightFileName, "%s/svm_weight_level31.csv", tUtil.dirPrefix );
		svmWrapper[2].SetDimensionCount(512);
		svmWrapper[2].Load( svmWeightFileName );
		sprintf( svmWeightFileName, "%s/svm_weight_level32.csv", tUtil.dirPrefix );		
		svmWrapper[3].SetDimensionCount(512);
		svmWrapper[3].Load( svmWeightFileName );
		sprintf( svmWeightFileName, "%s/svm_weight_level40.csv", tUtil.dirPrefix );
		svmWrapper[4].SetDimensionCount(512);
		svmWrapper[4].Load( svmWeightFileName );

		printf("[INFO] SVM Weights(Multi) are loaded\n");	
		fflush( stdout );	

		isOnceInited = true;
	}
	
}

void SVMRecognizer::trainLevelClassifier( char *baseDir, int levelCode )
{
	if( levelCode == 10 ) {
		cout << "Step : Level 1 Classifier trainning" << endl;
		trainClassifierLevel1( baseDir );
		cout << "Step : Level 1 Classifier trainning complete" << endl;
	} else if( levelCode == 20 ) {
		cout << "Step : Level 2 Classifier trainning" << endl;
		trainClassifierLevel2( baseDir );
		cout << "Step : Level 2 Classifier trainning complete" << endl;
	} else if( levelCode == 31 ) {
		cout << "Step : Level 3 Extra Classifier trainning" << endl;
		trainClassifierLevel3Etc( baseDir );
		cout << "Step : Level 3 Extra Classifier trainning complete" << endl;
	} else if( levelCode == 32 ) {
		cout << "Step : Level 3 Speedlimit Classifier trainning" << endl;
		trainClassifierLevel3SpeedLimit( baseDir );
		cout << "Step : Level 3 Speedlimit Classifier trainning complete" << endl;
	}
	


}

void SVMRecognizer::trainClassifierLevel1( char *baseDir )
{
	double dData[512];

	CLibSVMWrapper svm;	
	CLibSVMWrapper::_Node node;	
	node.m_pValues = dData;
	node.m_nLabel = 1;

	char buf[5000];
	FILE *fp;
	IplImage *img;		
	svm.SetDimensionCount(512);
	int arrSize = 0;
	IplImage *imgGray = cvCreateImage( cvSize(100,100), 8, 1 );	

	for( int i = 0; i < L1_CLASSES; i++ ) {
		sprintf( buf, "%s/SIGN_SRC/LEVEL_1/%s", baseDir, level1SubDir[i] );		
		vector<cv::String> classFileNames = ioUtil.getFileNames( buf, "jpg" );
		
		for( int j = 0; j < classFileNames.size(); j++ ) {	
			IplImage *roiImg = cvLoadImage( classFileNames[j].c_str(), 0 );		
			if( !roiImg ) continue;
			cvResize( roiImg, imgGray );

			
			// Step :: RSD 적용	
			CvPoint c;
			int r = -1;
			bool rsdSuccess = rsd.roiCalibration( imgGray, &c, &r );
			if( rsdSuccess ) {				
				IplImage *tmp = iUtil.getRoiFrame( imgGray, c.y-r, c.x-r, c.y+r, c.y+r, 0 );
				cvResize( tmp, imgGray );
				cvReleaseImage( &tmp );
			} 
			

			if( roiImg->height >= 15 ) {
				getVerticalByteMctHist( imgGray, mctDualHist );

				double* uHist = mctHistNormalization( mctDualHist[0] );
				double* lHist = mctHistNormalization( mctDualHist[1] );

				memset( dData, 0, sizeof(double)*512);
				for( int j = 0; j < 256; j++ ) {
					dData[ j ] = uHist[j];
					dData[ 256+j ] = lHist[j];				
				}

				node.m_nLabel = i;
				node.m_pValues = dData;
				svm.AddNode(node);			
			}
			cvReleaseImage( &roiImg );
			
			printf("\r\tClass %d :: [%d/%d] data processed...", i, (j+1) , classFileNames.size() );
			fflush(stdout);
		}
		printf("\n");

	}

	svm.Train();
	
	sprintf( buf, "%s\\svm_weight_level10.csv", tUtil.dirPrefix );
	svm.Save( buf );
	
}

void SVMRecognizer::trainClassifierLevel2( char *baseDir )
{
	double dData[512];

	CLibSVMWrapper svm;	
	CLibSVMWrapper::_Node node;	
	node.m_pValues = dData;
	node.m_nLabel = 1;

	char buf[5000];
	FILE *fp;
	IplImage *img;		
	svm.SetDimensionCount(512);
	int arrSize = 0;
	IplImage *imgGray = cvCreateImage( cvSize(100,100), 8, 1 );	
	
	for( int i = 0; i < L2_CLASSES; i++ ) {
		sprintf( buf, "%s/SIGN_SRC/LEVEL_2/%s", baseDir, level2SubDir[i] );		
		vector<cv::String> classFileNames = ioUtil.getFileNames( buf, "jpg" );

		for( int j = 0; j < classFileNames.size(); j++ ) {	
			IplImage *roiImg = cvLoadImage( classFileNames[j].c_str(), 0 );	
			if( !roiImg ) continue;
			cvResize( roiImg, imgGray );

			
			// Step :: RSD 적용	
			CvPoint c;
			int r = -1;
			bool rsdSuccess = rsd.roiCalibration( imgGray, &c, &r );
			if( rsdSuccess ) {				
				IplImage *tmp = iUtil.getRoiFrame( imgGray, c.y-r, c.x-r, c.y+r, c.y+r, 0 );
				cvResize( tmp, imgGray );
				cvReleaseImage( &tmp );
			} 
			

			if( roiImg->height >= 15 ) {
				getVerticalByteMctHist( imgGray, mctDualHist );

				double* uHist = mctHistNormalization( mctDualHist[0] );
				double* lHist = mctHistNormalization( mctDualHist[1] );

				memset( dData, 0, sizeof(double)*512);
				for( int j = 0; j < 256; j++ ) {
					dData[ j ] = uHist[j];
					dData[ 256+j ] = lHist[j];				
				}

				node.m_nLabel = i;
				node.m_pValues = dData;
				svm.AddNode(node);			
			}
			cvReleaseImage( &roiImg );

			printf("\r\tClass %d :: [%d/%d] data processed...", i, (j+1) , classFileNames.size() );
			fflush(stdout);
		}
		printf("\n");

	}

	svm.Train();

	sprintf( buf, "%s\\svm_weight_level20.csv", tUtil.dirPrefix );
	svm.Save( buf );
	
}

void SVMRecognizer::trainClassifierLevel3Etc( char *baseDir )
{
	double dData[512];

	CLibSVMWrapper svm;	
	CLibSVMWrapper::_Node node;	
	node.m_pValues = dData;
	node.m_nLabel = 1;

	char buf[5000];
	FILE *fp;
	IplImage *img;		
	svm.SetDimensionCount(512);
	int arrSize = 0;
	IplImage *imgGray = cvCreateImage( cvSize(100,100), 8, 1 );	

	for( int i = 0; i < L3_ETC_CLASSES; i++ ) {
		sprintf( buf, "%s/SIGN_SRC/LEVEL_3_ETC/%s", baseDir, level3EtcSubDir[i] );		
		vector<cv::String> classFileNames = ioUtil.getFileNames( buf, "jpg" );

		for( int j = 0; j < classFileNames.size(); j++ ) {	
			IplImage *roiImg = cvLoadImage( classFileNames[j].c_str(), 0 );	
			if( !roiImg ) continue;
			cvResize( roiImg, imgGray );

			
			// Step :: RSD 적용	
			CvPoint c;
			int r = -1;
			bool rsdSuccess = rsd.roiCalibration( imgGray, &c, &r );
			if( rsdSuccess ) {				
				IplImage *tmp = iUtil.getRoiFrame( imgGray, c.y-r, c.x-r, c.y+r, c.y+r, 0 );
				cvResize( tmp, imgGray );
				cvReleaseImage( &tmp );
			} 
			

			if( roiImg->height >= 15 ) {
				getHorizontalByteMctHist( imgGray, mctDualHist );

				double* lHist = mctHistNormalization( mctDualHist[0] );
				double* rHist = mctHistNormalization( mctDualHist[1] );

				memset( dData, 0, sizeof(double)*512);
				for( int j = 0; j < 256; j++ ) {
					dData[ j ] = lHist[j];
					dData[ 256+j ] = rHist[j];				
				}

				node.m_nLabel = i;
				node.m_pValues = dData;
				svm.AddNode(node);			
			}
			cvReleaseImage( &roiImg );

			printf("\r\tClass %d :: [%d/%d] data processed...", i, (j+1) , classFileNames.size() );
			fflush(stdout);
		}
		printf("\n");

	}

	svm.Train();

	sprintf( buf, "%s\\svm_weight_level31.csv", tUtil.dirPrefix );
	svm.Save( buf );

}

void SVMRecognizer::trainClassifierLevel3SpeedLimit( char *baseDir )
{
	double dData[512];

	CLibSVMWrapper svm;	
	CLibSVMWrapper::_Node node;	
	node.m_pValues = dData;
	node.m_nLabel = 1;

	char buf[5000];
	FILE *fp;
	IplImage *img;		
	svm.SetDimensionCount(512);
	int arrSize = 0;
	IplImage *imgGray = cvCreateImage( cvSize(100,100), 8, 1 );	

	for( int i = 0; i < L3_SPEEDLIMIT_CLASSES; i++ ) {
		sprintf( buf, "%s/SIGN_SRC/LEVEL_3_SPEEDLIMIT/%s", baseDir, level3SpeedLimitSubDir[i] );		
		vector<cv::String> classFileNames = ioUtil.getFileNames( buf, "jpg" );

		for( int j = 0; j < classFileNames.size(); j++ ) {	
			IplImage *roiImg = cvLoadImage( classFileNames[j].c_str(), 0 );	
			if( !roiImg ) continue;
			cvResize( roiImg, imgGray );

			
			// Step :: RSD 적용	
			CvPoint c;
			int r = -1;
			bool rsdSuccess = rsd.roiCalibration( imgGray, &c, &r );
			if( rsdSuccess ) {				
				IplImage *tmp = iUtil.getRoiFrame( imgGray, c.y-r, c.x-r, c.y+r, c.y+r, 0 );
				cvResize( tmp, imgGray );
				cvReleaseImage( &tmp );
			} 
			

			if( roiImg->height >= 15 ) {
				getQuadByteMctHist( imgGray, mctQuadHist );

				double* luHist = mctHistNormalization( mctQuadHist[0] );
				double* ruHist = mctHistNormalization( mctQuadHist[1] );
				double* llHist = mctHistNormalization( mctQuadHist[2] );
				double* rlHist = mctHistNormalization( mctQuadHist[3] );

				memset( dData, 0, sizeof(double)*512);
				for( int j = 0; j < 256; j++ ) {
					dData[ j ] = luHist[j];
					dData[ 256+j ] = llHist[j];
					//dData[ 512+j ] = llHist[j];
					//dData[ 768+j ] = rlHist[j];
				}

				node.m_nLabel = i;
				node.m_pValues = dData;
				svm.AddNode(node);

				free(luHist);
				free(ruHist);
				free(llHist);
				free(rlHist);
			}
			cvReleaseImage( &roiImg );

			printf("\r\tClass %d :: [%d/%d] data processed...", i, (j+1) , classFileNames.size() );
			fflush(stdout);
		}
		printf("\n");

	}

	svm.Train();

	sprintf( buf, "%s\\svm_weight_level32.csv", tUtil.dirPrefix );
	svm.Save( buf );

}

bool SVMRecognizer::classifyRoi( IplImage *roiImg, int &classNo, double &prob ) {
	bool bResult = false;

	// Step :: Classify Level 1 Verifier
	classifyLevel1( roiImg, classNo, prob );

	if( classNo == 0 ) {
		// Case :: Verified Sign ROI
		bResult = true;
	
		// Step :: Classify Level 2
		classifyLevel2( roiImg, classNo, prob );
		
		//cout << "\tLevel 2 :: " << classNo << ", Prob :: " << prob << endl;

		if( classNo == 0 && prob > 0.4 ) {
			// Step :: Classify Level 3 SpeedLimit
			classifyLevel3SpeedLimit( roiImg, classNo, prob );
			
			if( classNo == 7 ) {
				// Case :: Class 30, 60, 80, 90
				//classifyLevel4SpeedLimit( roiImg, classNo, prob );
				if( classNo == 0 ) classNo = 0;
				else if( classNo == 1 ) classNo = 3;
				else if( classNo == 2 ) classNo = 5;				
				else if( classNo == 3 ) classNo = 6;

				//cout << "\tLevel 4 SpeedLimit" << nClass << endl;
			} else {
				// Case :: Class SpeedLimit Normal
				if( classNo == 0 ) classNo = 1;			// 40
				else if( classNo == 1 ) classNo = 2;	// 50
				else if( classNo == 2 ) classNo = 4;	// 70
				else if( classNo == 3 ) classNo = 7;	// 100
				else if( classNo == 4 ) classNo = 8;	// 110
				else if( classNo == 5 ) classNo = 9;	// 120
				else if( classNo == 6 ) classNo = 10;	// 130			

				//cout << "\tLevel 3 SpeedLimit " << nClass << endl;
			}
		} else {
			// Step :: Classify Level 3 Extra
			classifyLevel3Etc( roiImg, classNo, prob );
			classNo = L2_SPEEDLIMIT_CLASSES + classNo;			
		}
	}

	
	return bResult;
}

void SVMRecognizer::classifyLevel1( IplImage *roiImg, int &classNo, double &prob ) {

	double dData[512];

	IplImage *imgGray = cvCreateImage( cvSize(100,100), 8, 1 );	
	classNo = -1;

	cvResize( roiImg, imgGray );
	getVerticalByteMctHist( imgGray, mctDualHist );

	double* uHist = mctHistNormalization( mctDualHist[0] );
	double* lHist = mctHistNormalization( mctDualHist[1] );

	memset( dData, 0, sizeof(double)*512);
	for( int j = 0; j < 256; j++ ) {
		dData[ j ] = uHist[j];
		dData[ 256+j ] = lHist[j];				
	}

	CLibSVMWrapper::_Node node;	
	node.m_pValues = dData;
	node.m_nLabel = -1;
	node.m_pValues = dData;
	double probs[L1_CLASSES] = {0.0, };
	svmWrapper[0].Predict( node, probs );

	classNo = node.m_nLabel;
	prob = probs[ node.m_nLabel ];

	//printf("Level 1 Verifier, Class : %d, Probs : %f, %f\n", node.m_nLabel, probs[0], probs[1] );
	cvReleaseImage( &imgGray );

}

void SVMRecognizer::classifyLevel2( IplImage *roiImg, int &classNo, double &prob ) {

	double dData[512];

	IplImage *imgGray = cvCreateImage( cvSize(100,100), 8, 1 );	
	classNo = -1;

	cvResize( roiImg, imgGray );
	getVerticalByteMctHist( imgGray, mctDualHist );

	double* uHist = mctHistNormalization( mctDualHist[0] );
	double* lHist = mctHistNormalization( mctDualHist[1] );

	memset( dData, 0, sizeof(double)*512);
	for( int j = 0; j < 256; j++ ) {
		dData[ j ] = uHist[j];
		dData[ 256+j ] = lHist[j];				
	}

	CLibSVMWrapper::_Node node;	
	node.m_pValues = dData;
	node.m_nLabel = -1;
	node.m_pValues = dData;
	double probs[L2_CLASSES] = {0.0, };
	svmWrapper[1].Predict( node, probs );

	classNo = node.m_nLabel;
	prob = probs[ classNo ];

	//printf("Class : %d, Probs : %f, %f\n", node.m_nLabel, probs[0], probs[1] );
	cvReleaseImage( &imgGray );

}

void SVMRecognizer::classifyLevel3Etc( IplImage *roiImg, int &classNo, double &prob ) {

	double dData[512];

	IplImage *imgGray = cvCreateImage( cvSize(100,100), 8, 1 );	
	classNo = -1;

	cvResize( roiImg, imgGray );
	getHorizontalByteMctHist( imgGray, mctDualHist );

	double* lHist = mctHistNormalization( mctDualHist[0] );
	double* rHist = mctHistNormalization( mctDualHist[1] );

	memset( dData, 0, sizeof(double)*512);
	for( int j = 0; j < 256; j++ ) {
		dData[ j ] = lHist[j];
		dData[ 256+j ] = rHist[j];				
	}

	CLibSVMWrapper::_Node node;	
	node.m_pValues = dData;
	node.m_nLabel = -1;
	node.m_pValues = dData;
	double probs[L3_ETC_CLASSES] = {0.0, };
	svmWrapper[2].Predict( node, probs );

	classNo = node.m_nLabel;
	prob = probs[ classNo ];

	//printf("Class : %d, Probs : %f, %f\n", node.m_nLabel, probs[0], probs[1] );
	cvReleaseImage( &imgGray );

}

void SVMRecognizer::classifyLevel3SpeedLimit( IplImage *roiImg, int &classNo, double &prob ) {

	double dData[512];

	IplImage *imgGray = cvCreateImage( cvSize(100,100), 8, 1 );	
	classNo = -1;

	cvResize( roiImg, imgGray );
	getQuadByteMctHist( imgGray, mctQuadHist );

	double* luHist = mctHistNormalization( mctQuadHist[0] );
	double* ruHist = mctHistNormalization( mctQuadHist[1] );
	double* llHist = mctHistNormalization( mctQuadHist[2] );
	double* rlHist = mctHistNormalization( mctQuadHist[3] );

	memset( dData, 0, sizeof(double)*512);
	for( int j = 0; j < 256; j++ ) {
		dData[ j ] = luHist[j];
		dData[ 256+j ] = llHist[j];
		//dData[ 512+j ] = llHist[j];
		//dData[ 768+j ] = rlHist[j];
	}
	
	free(luHist);
	free(ruHist);
	free(llHist);
	free(rlHist);

	CLibSVMWrapper::_Node node;	
	node.m_pValues = dData;
	node.m_nLabel = -1;
	node.m_pValues = dData;
	double probs[L3_SPEEDLIMIT_CLASSES] = {0.0, };
	svmWrapper[3].Predict( node, probs );

	classNo = node.m_nLabel;
	prob = probs[ classNo ];

	//printf("Class : %d, Probs : %f, %f\n", node.m_nLabel, probs[0], probs[1] );
	cvReleaseImage( &imgGray );

}

double* SVMRecognizer::mctHistNormalization( int* hist ) {
	const int binSize = pow( 2.0, 8 );	
	int maxValue = -1;
	double* nHist = (double*)malloc(sizeof(double)*256);

	// Step :: Max 값 검색
	for( int i = 0; i < binSize; i++ ) {		
		if( hist[i] > maxValue ) maxValue = hist[i];		
	}
	if( maxValue != 0 )
		for( int j = 0; j < binSize; j++ ) {
			nHist[j] = (double)hist[j] / (double)maxValue;			
		}
	else
		for( int j = 0; j < binSize; j++ ) {
			nHist[j] = (double)hist[j];
		}
		
	return nHist;
}

void SVMRecognizer::getVerticalByteMctHist( IplImage *gray, int **mctHist) {

	for( int i = 0; i < 256; i++ ) {
		for( int x = 0; x < 2; x++ ) {
			mctHist[x][i] = 0;
		}	
	}

	int featQty = 0;
	int *mctFeat = mctGen.get8BitMctFeatIndices( gray , &featQty );	

	for( int r = 0; r < gray->height-2; r++ ) {
		for( int c = 0; c < gray->width-2; c++ ) {
			int mctIdx = ( r ) * ( gray->width - 2 ) + ( c );

			if( r < gray->height / 2 ) {
				// Case :: 이미지 위 부분
				mctHist[0][ mctFeat[ mctIdx ] ] += 1;				
			} else if ( r > gray->height / 2 ){
				// Case :: 이미지 왼쪽 아래 부분
				mctHist[1][ mctFeat[ mctIdx ] ] += 1;				
			}
		}
	}
	free( mctFeat );
}

void SVMRecognizer::getHorizontalByteMctHist( IplImage *gray, int **mctHist) {

	for( int i = 0; i < 256; i++ ) {
		for( int x = 0; x < 2; x++ ) {
			mctHist[x][i] = 0;
		}	
	}

	int featQty = 0;
	int *mctFeat = mctGen.get8BitMctFeatIndices( gray , &featQty );	

	for( int r = 0; r < gray->height-2; r++ ) {
		for( int c = 0; c < gray->width-2; c++ ) {
			int mctIdx = ( r ) * ( gray->width - 2 ) + ( c );

			if( c < gray->width / 2 ) {
				// Case :: 이미지 왼쪽 부분
				mctHist[0][ mctFeat[ mctIdx ] ] += 1;				
			} else if ( c > gray->width / 2 ){
				// Case :: 이미지 오른쪽 부분
				mctHist[1][ mctFeat[ mctIdx ] ] += 1;				
			}
		}
	}
	free( mctFeat );
}

void SVMRecognizer::getQuadByteMctHist( IplImage *gray, int **mctHist) {

	for( int i = 0; i < 256; i++ ) {
		for( int x = 0; x < 4; x++ ) {
			mctHist[x][i] = 0;
		}	
	}

	
//	Quad 구성
//	0 1
//	2 3
	

	int featQty = 0;
	int *mctFeat = mctGen.get8BitMctFeatIndices( gray , &featQty );	

	for( int r = 0; r < gray->height-2; r++ ) {
		for( int c = 0; c < gray->width-2; c++ ) {
			int mctIdx = ( r ) * ( gray->width - 2 ) + ( c );

			if( c < gray->width / 2 ) {
				// Case :: 이미지 왼쪽 부분
				if( r < gray->height / 2 ) {
					// Case :: 0
					mctHist[0][ mctFeat[ mctIdx ] ] += 1;				
				} else {
					// Case :: 2
					mctHist[2][ mctFeat[ mctIdx ] ] += 1;				
				}
			} else if ( c > gray->width / 2 ){
				// Case :: 이미지 오른쪽 부분
				if( r > gray->height / 2 ) {
					// Case :: 1
					mctHist[1][ mctFeat[ mctIdx ] ] += 1;				
				} else {
					// Case :: 3
					mctHist[3][ mctFeat[ mctIdx ] ] += 1;				
				}
			}
		}
	}
	free( mctFeat );
}
*/