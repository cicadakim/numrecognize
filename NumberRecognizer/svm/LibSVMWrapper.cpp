#include "stdAfx.h"
#include <assert.h>
#include "LibSVMWrapper.h"


//////////////////////////////////////////////////////////////////////////
//	Lib Link
//////////////////////////////////////////////////////////////////////////
#ifndef _LINK_LIBSVM
#define _LINK_LIBSVM
#pragma comment(lib,"libsvm.lib")
#pragma message("libsvm.lib is linked ")
#endif	//#ifndef _LINK_LADYBUG

using namespace Classifier;

CLibSVMWrapper::CLibSVMWrapper(void)
{
	m_pModel	= NULL;
	m_pXSpace	= NULL;
	
	memset(&m_Param,0,sizeof(m_Param));
	memset(&m_Prob,0,sizeof(m_Prob));
}


CLibSVMWrapper::~CLibSVMWrapper(void)
{
	ClearNodes();
	Release();
}


void CLibSVMWrapper::AddNode(const _Node& node)
{
	assert(0 < m_nDimensionCount);
	_Node NewNode;
	NewNode.m_pValues = new double[m_nDimensionCount];
	memcpy(NewNode.m_pValues,node.m_pValues,sizeof(double)* m_nDimensionCount);
	NewNode.m_nLabel	= node.m_nLabel;
	m_pNodes.push_back(NewNode);
}

void CLibSVMWrapper::ClearNodes()
{
	std::vector<_Node>::iterator it		= m_pNodes.begin();
	std::vector<_Node>::iterator itE	= m_pNodes.end();

	for(; it!= itE; it++)
	{
		_Node node = *it;
		if(node.m_pValues)	delete[] node.m_pValues;
	}
	m_pNodes.clear();
}
void CLibSVMWrapper::Predict(_Node& node)
{
	assert(0 < m_nDimensionCount);
	assert(m_pModel != NULL);

	svm_node* pNode = new svm_node[m_nDimensionCount+1];
	for(int iDim = 0; iDim < m_nDimensionCount; iDim++)
	{
		pNode[iDim].index = iDim+1;
		pNode[iDim].value = node.m_pValues[iDim];
	}
	pNode[m_nDimensionCount].index = -1;
	double d = svm_predict(m_pModel,pNode);
	delete []pNode;
	node.m_nLabel = d;

}

void CLibSVMWrapper::Predict(_Node& node, double *prob)
{
	assert(0 < m_nDimensionCount);
	assert(m_pModel != NULL);

	svm_node* pNode = new svm_node[m_nDimensionCount+1];
	for(int iDim = 0; iDim < m_nDimensionCount; iDim++)
	{
		pNode[iDim].index = iDim+1;
		pNode[iDim].value = node.m_pValues[iDim];
	}
	pNode[m_nDimensionCount].index = -1;		
	double d = svm_predict_probability(m_pModel, pNode,prob);
	delete []pNode;
	node.m_nLabel = d;	
}

void CLibSVMWrapper::Release()
{
	if(m_pXSpace)	
	{
		delete[] m_pXSpace;
		m_pXSpace = NULL;
	}
	if(m_Prob.x)
	{
		delete[] m_Prob.x;
		m_Prob.x	= NULL;
	}
	if(m_Prob.y)
	{
		delete[] m_Prob.y;
		m_Prob.y	= NULL;
	}
	free(m_Param.weight_label);
	free(m_Param.weight);

	if(m_pModel)	svm_free_and_destroy_model(&m_pModel);

}

void CLibSVMWrapper::Train()
{
	assert(m_pNodes.empty() == FALSE);
	assert(0 < m_nDimensionCount);
	
	Release();

	m_Param.svm_type = C_SVC;
	//m_Param.svm_type = NU_SVC;
	m_Param.kernel_type = RBF;
	m_Param.degree = 3;
	m_Param.gamma = 1;
	m_Param.coef0 = 0;
	m_Param.nu = 0.5;
	m_Param.cache_size = 100;
	m_Param.C = 100;
	m_Param.eps = 1e-3;
	m_Param.p = 0.1;
	m_Param.shrinking = 1;
	m_Param.probability = 1;
	m_Param.nr_weight = 0;
	m_Param.weight_label = NULL;
	m_Param.weight = NULL;

	const int nSvmNodeStep = m_nDimensionCount+1;

	m_Prob.l = m_pNodes.size();
	m_Prob.y = new double[m_Prob.l];
	m_pXSpace = new svm_node[nSvmNodeStep*m_Prob.l];
	m_Prob.x = new svm_node* [m_Prob.l];

	for(int i = 0; i < m_pNodes.size(); i++)
	{
		_Node Node = m_pNodes[i];
		
		for(int iDim = 0; iDim <m_nDimensionCount; iDim++)
		{
			m_pXSpace[nSvmNodeStep*i+iDim].index = iDim+1;
			m_pXSpace[nSvmNodeStep*i+iDim].value = Node.m_pValues[iDim];
		}
		m_pXSpace[nSvmNodeStep*i+m_nDimensionCount].index	= -1;
		m_Prob.x[i] = &m_pXSpace[nSvmNodeStep*i];
		m_Prob.y[i] = Node.m_nLabel;
	}

	m_pModel = svm_train(&m_Prob, &m_Param);
}

void CLibSVMWrapper::Load(const char* strPathFile)
{
	if(m_pModel)	svm_free_and_destroy_model(&m_pModel);

	m_pModel	= svm_load_model(strPathFile);
    

}

void CLibSVMWrapper::Save(const char* strPathFile)
{
	assert(m_pModel != NULL);
	svm_save_model(strPathFile,m_pModel);
}
