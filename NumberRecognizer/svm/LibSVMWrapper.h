#pragma once
/****************************************************************************************
* Include files
****************************************************************************************/
#include "svm.h"
#include <atlconv.h>
#include <vector>

/*!
 * \brief	: 분류를 위한 항목
 */
namespace Classifier
{
	/*!
	 * \brief	: LibSVM을 이용한 SVM 분류, 해당 API를 Wrapping 했다.
	 * \remarks
	 */
	class CLibSVMWrapper
	{
	public:
		
		/*!
		 * \brief	: Data Node
		 * \remark	: m_pValues는 Data의 Dimension을 Check하지 않고 일괄적으로 읽어온다. 이에 따른 Memory영역의 침범 조심
		 */
		struct _Node
		{
			_Node(double* pValues = NULL,int nLabel= 0)
			{	m_pValues = pValues;	m_nLabel = nLabel;}
			double* m_pValues;	///> Dimension에 따른 Data값(행렬)	
			int		m_nLabel;	///> 구분자ID
		};
		CLibSVMWrapper(void);
		~CLibSVMWrapper(void);
		
		/*!
		 * \brief		: Data의 Dimension 설정 \r\n
		 *					예를 들어, 좌표와 같은 2차원(X,Y) 데이터는 2로, \r\n
		 *					몸무게,키,나이로 이루어진 데이터는 3으로 설정해야한다.
		 * \param nDimensionCount	: [in]설정할 값
		 * \remarks	: AddNode하기 전에 미리 값이 설정되어 있어야 한다.
		 */
		void SetDimensionCount(int nDimensionCount)	{ m_nDimensionCount = nDimensionCount;}

		/*!
		 * \brief		: 학습할 DataNode 추가
		 * \param node	
		 */
		void AddNode(const _Node& node);

		/*!
		 * \brief		: 분류
		 * \param node	: [in/out]분류할 데이터
		 * \remark		: 분류된 결과는	node.m_nLabel에 담겨진다.
		 */
		void Predict(_Node& node);

		/*!
		 * \brief		: 분류
		 * \param node	: [in/out]분류할 데이터
		 * \remark		: 분류된 결과는	node.m_nLabel에 담겨진다.
		 */
		void Predict(_Node& node, double *prob);
		
		/*!
		 * \brief	: 학습
		 */
		void Train();

		/*!
		 * \brief	:	학습된 내용을 Loading
		 */
		void Load(const char* strPathFile);
		
		/*!
		 * \brief	: 학습된 내용을 Save
		 */
		void Save(const char* strPathFile);
		
	protected:
		void Release();
		void ClearNodes();
		int m_nDimensionCount;			
		svm_model*			m_pModel;
		std::vector<_Node>	m_pNodes;
		svm_parameter		m_Param;
		svm_problem			m_Prob;
		svm_node*			m_pXSpace;
		
	};
};	//namespace Classifier



