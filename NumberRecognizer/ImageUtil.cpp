#include "stdafx.h"
#include "ImageUtil.h"

using namespace cv;


/**
@class   ImageUtil
@이미지 처리 관련 편의 기능을 구현한 클래스.
*/
	IplImage* ImageUtil::getRoiFrame( IplImage *frame, CvRect rect )
	{
		return getRoiFrame( frame, rect.y, rect.x, rect.y+rect.height, rect.x+rect.width, 0 );
	}

	IplImage* ImageUtil::getRoiFrame( IplImage *frame, int minR, int minC, int maxR, int maxC, int radius )
	{

		minR = ( minR - radius ) >= 0 ? minR - radius : 0;
		maxR = ( maxR + radius ) <= frame->height ? maxR + radius : frame->height;
		minC = ( minC - radius - 1 ) >= 0 ? minC - radius - 1 : 0;
		maxC = ( maxC + radius ) <= frame->widthStep ? maxC + radius : frame->widthStep;

		if( maxR - minR <= 0 || maxC - minC <= 0 )
			return NULL;

		IplImage *dst = cvCreateImage( cvSize( maxC-minC, maxR-minR ), IPL_DEPTH_8U, frame->nChannels );
		cvZero( dst );

		for ( int r = minR ; r < maxR; r++ ) {
			for ( int c = minC ; c < maxC; c++ ) {

				int srcFrameIdx = r * frame->widthStep + c * frame->nChannels;					
				int calFrameIdx = ( r - minR ) * dst->widthStep + ( c - minC ) * dst->nChannels;

				dst->imageData[ calFrameIdx + 0 ] = (unsigned char)frame->imageData[ srcFrameIdx + 0 ];

				if( frame->nChannels > 1 ) {
					dst->imageData[ calFrameIdx + 1 ] = (unsigned char)frame->imageData[ srcFrameIdx + 1 ];
					dst->imageData[ calFrameIdx + 2 ] = (unsigned char)frame->imageData[ srcFrameIdx + 2 ];
				}

			}
		}	
		return dst;
	}

	double ImageUtil::angle( cv::Point pt1, cv::Point pt2, cv::Point pt0 )
	{
		double dx1 = pt1.x - pt0.x;
		double dy1 = pt1.y - pt0.y;
		double dx2 = pt2.x - pt0.x;
		double dy2 = pt2.y - pt0.y;
		return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
	}

	void ImageUtil::drawDottedLine(IplImage* img,CvPoint pt1, CvPoint pt2,CvScalar color, int thickness, int lenghOfDots, int lineType, int leftToRight) 
	{ 
		CvLineIterator iterator; 
		int count = cvInitLineIterator( img, pt1, pt2, &iterator, lineType, leftToRight );	
		int offset,x,y; 


		for( int i = 0; i < count; i= i + (lenghOfDots*2-1) ) 
		{ 
			if(i+lenghOfDots > count) 
				break; 

			offset = iterator.ptr - (uchar*)(img->imageData); 
			y = offset/img->widthStep; 
			x = (offset - y*img->widthStep)/(3*sizeof(uchar) /* size of pixel */); 

			CvPoint lTemp1 = cvPoint(x,y); 
			for(int j=0;j<lenghOfDots-1;j++)	//I want to know have the last of these in the iterator 
				CV_NEXT_LINE_POINT(iterator); 

			offset = iterator.ptr - (uchar*)(img->imageData); 
			y = offset/img->widthStep; 
			x = (offset - y*img->widthStep)/(3*sizeof(uchar) /* size of pixel */); 

			CvPoint lTemp2 = cvPoint(x,y); 
			cvDrawLine(img,lTemp1,lTemp2,color,thickness,lineType); 
			for(int j=0;j<lenghOfDots;j++) 
				CV_NEXT_LINE_POINT(iterator); 
		} 
	} 

	void ImageUtil::drawDottedRect(IplImage* img,CvPoint pt1, CvPoint pt2,CvScalar color, int thickness, int lenghOfDots , int lineType ) 
	{	//1---2 
		//|	 | 
		//4---3 
		//	1 --> pt1, 2 --> tempPt1, 3 --> pt2, 4 --> tempPt2 

		CvPoint tempPt1 = cvPoint(pt2.x,pt1.y); 
		CvPoint tempPt2 = cvPoint(pt1.x,pt2.y); 
		drawDottedLine(img,pt1,tempPt1,color,thickness,lenghOfDots,lineType, 0); 
		drawDottedLine(img,tempPt1,pt2,color,thickness,lenghOfDots,lineType, 0); 
		drawDottedLine(img,pt2,tempPt2,color,thickness,lenghOfDots,lineType, 1); 
		drawDottedLine(img,tempPt2,pt1,color,thickness,lenghOfDots,lineType, 1); 
	} 

	void ImageUtil::drawDottedRect(IplImage* img,CvRect rect,CvScalar color, int thickness , int lenghOfDots , int lineType ) 
	{	//1---2 
		//|	 | 
		//4---3 
		CvPoint pt1 = cvPoint(rect.x,rect.y); 
		CvPoint pt2 = cvPoint(rect.x+rect.width,rect.y); 
		CvPoint pt3 = cvPoint(rect.x+rect.width,rect.y+rect.height); 
		CvPoint pt4 = cvPoint(rect.x,rect.y+rect.height); 

		drawDottedLine(img,pt1,pt2,color,thickness,lenghOfDots,lineType, 0); 
		drawDottedLine(img,pt2,pt3,color,thickness,lenghOfDots,lineType, 0); 
		drawDottedLine(img,pt3,pt4,color,thickness,lenghOfDots,lineType, 1); 
		drawDottedLine(img,pt4,pt1,color,thickness,lenghOfDots,lineType, 1); 
	}

	int ImageUtil::getAvgIntensity( IplImage *gray )
	{
		int idx = 0;
		long sum = 0;

		for( int r = 0; r < gray->height; r++ ) {
			for( int c = 0; c < gray->width; c++ ) {
				idx = r * gray->widthStep + c;
				sum += (uchar)gray->imageData[idx];
			}
		}
		return sum / ( gray->width * gray->height );
	}

	void ImageUtil::setRoi( IplImage *src, IplImage *dst, CvPoint pt )
	{
		if( pt.x + src->width > dst->width )
			pt.x -= ( pt.x + src->width ) - dst->width;
		if( pt.y + src->height > dst->height )
			pt.y -= ( pt.y + src->height ) - dst->height;

		for( int r = 0; r < src->height; r++ ) {
			for( int c = 0; c < src->width; c++ ) {
				int idxA = r * src->widthStep + c * src->nChannels;
				int idxB = ( r + pt.y ) * dst->widthStep + ( c + pt.x ) * dst->nChannels;

				dst->imageData[ idxB ] = (uchar)src->imageData[ idxA ];
				if( src->nChannels == 3 && dst->nChannels == 3 ) {
					dst->imageData[ idxB + 1 ] = (uchar)src->imageData[ idxA + 1 ];
					dst->imageData[ idxB + 2 ] = (uchar)src->imageData[ idxA + 2 ];
				}
			}
		}
	}
