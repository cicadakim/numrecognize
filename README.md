# README #

# tesseract와 opencv를 이용한 이미지 내 숫자 추출

# 실행
NumRecognize\Bin64\NumberRecognizer.exe

# 실험용 이미지
NumRecognize\testImage

# 사용 방법
Open Image버튼으로 이미지를 불러온 후 단축키와 마우스로 조작

단축키

    'n' : 기본상태. 검출된 숫자 후보군을 화면에 출력

    'a' : 모든 문자 후보군을 화면에 출력

    ' ' : tesseract가 실제로 인식하는 전처리된 gray Image를 출력

    's' : 원본 이미지 출력

    엔터 : 현재 선택된 숫자 후보군을 추출함(원본 이미지가 있는 폴더에 extract폴더를 만들어서 추출)

마우스

    왼쪽클릭 & 드래그 : 해당하는 사각형 영역을 숫자 후보군으로 지정함

    오른쪽 클릭 : 해당 포인트에 사각형이 있을경우 해제하고, 없을경우 해당 문자를 숫자 후보군으로 지정함(오인식 가능성 존재)

# 동작 과정
Open Image버튼 누른뒤 이미지 선택 -> 자동으로 숫자 후보군이 검출된 모습
![제목 없음.png](https://bitbucket.org/repo/ek5rp8b/images/1400088934-%EC%A0%9C%EB%AA%A9%20%EC%97%86%EC%9D%8C.png)
이 중 오인식된 부분을 제거하고, 인식이 되지 않은 숫자를 박싱함
![2.png](https://bitbucket.org/repo/ek5rp8b/images/3759856972-2.png)
엔터를 눌러서 숫자를 추출
![3.png](https://bitbucket.org/repo/ek5rp8b/images/3611978381-3.png)