
// NumberRecognizerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "NumberRecognizer.h"
#include "NumberRecognizerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CNumberRecognizerDlg 대화 상자



CNumberRecognizerDlg::CNumberRecognizerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CNumberRecognizerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_ptrArray1 = NULL;
	m_nIndex = 0;
}

void CNumberRecognizerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CNumberRecognizerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON5, &CNumberRecognizerDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON1, &CNumberRecognizerDlg::OnBnClickedButtonOpenImage)
	ON_BN_CLICKED(IDC_BUTTON2, &CNumberRecognizerDlg::OnBnClickedButtonDetectCharacter)
	ON_BN_CLICKED(IDC_BUTTON3, &CNumberRecognizerDlg::OnBnClickedButtonTrainSVM)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CNumberRecognizerDlg 메시지 처리기

BOOL CNumberRecognizerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CNumberRecognizerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CNumberRecognizerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CNumberRecognizerDlg::OnBnClickedButton5()
{
	// TODO: Add your control notification handler code here
	exit(0);
}


void CNumberRecognizerDlg::OnBnClickedButtonOpenImage()
{
	SelectOpenImage();
	//SelectFolderAndDetectAll();	
}

void CNumberRecognizerDlg::SelectOpenImage()
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	CString imgFileName;	

	// File dialog
	char szFilter[] = "All Images( *.jpg, *.png, *.tif, *.* ) | *.jpg; *.JPG; *.png; *.PNG; *.tif; *.TIF;*.* || ";
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);
	if( IDOK == dlg.DoModal() ){
		imgFileName = dlg.GetPathName();		
	}

	// File Validation
	{	
		FILE *fp;
		fopen_s( &fp, imgFileName, "r" );
		if( fp == NULL ) {
			AfxMessageBox("파일을 로드할 수 없음.");
			return;
		}
		fclose(fp);
	}
	UpdateData(TRUE);
	cout << imgFileName<<endl;
	srcImage  = cvLoadImage( imgFileName );

	cvShowImage("Source", srcImage);
	
	cvWaitKey(10);
}

void CNumberRecognizerDlg::SelectFolderAndDetectAll()
{
	nameNumbering = 0;
	//Select Folder
	
	UpdateData(TRUE);

	CString dirPath;	

	// File dialog
	char szFilter[] = "All Images( *.jpg, *.png, *.tif, *.* ) | *.jpg; *.JPG; *.png; *.PNG; *.tif; *.TIF;*.* || ";
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);
	if( IDOK == dlg.DoModal() ){
		dirPath = dlg.GetPathName();		
	}
	
	int dirIndex = dirPath.ReverseFind('\\');
	if(dirIndex==-1)return;
	dirPath = dirPath.Mid(0,dirIndex);
	
	cout << dirPath << endl;

	_finddatai64_t c_file;
	intptr_t hFile;

	if ((hFile = _findfirsti64(dirPath+"\\*.jpg", &c_file)) == -1L) {
		switch (errno) {
		case ENOENT:
			printf(":: 파일이 없음 ::\n"); break;
		case EINVAL:
			fprintf(stderr, "Invalid path name.\n"); exit(1); break;
		case ENOMEM:
			fprintf(stderr, "Not enough memory or file name too long.\n"); exit(1); break;
		default:
			fprintf(stderr, "Unknown error.\n"); exit(1); break;
		}
	} // end if
	else {
		printf("file detecting\n");
		do {
			UpdateData(TRUE);
			srcImage = cvLoadImage(dirPath + "\\" + c_file.name);
			cout<<dirPath + "\\" + c_file.name<<endl;
			DetectCharacterWithOpenImage();
		} while (_findnexti64(hFile, &c_file) == 0);
		_findclose(hFile); // _findfirsti64(), _findnexti64()에 사용된 메모리를 반환
	} // end else
	
}

void CNumberRecognizerDlg::DetectCharacterWithOpenImage()
{
	Mat matImg;

	//Mat matImg = imread((String)strAlt);
	//Mat matImg;
	matImg = cvarrToMat( srcImage );

	Mat matBlurImage;
	Mat matResult;

	// Noise 제거
	/////////////////////////////////////////////////////////////////////////////
	// 1. 노이즈 제거
	/////////////////////////////////////////////////////////////////////////////
	medianBlur(matImg, matBlurImage, 3 );
	if ( !matImg.empty() ){
		
		Mat matMiddle;
		Mat matTemp;
					int i, j, k, m, n;
		int nLine = 0;
		int nContinue = 0;

		if ( m_ptrArray1 != NULL )
			m_ptrArray1->RemoveAll( );
		m_ptrArray1 = new CPtrArray;

		double dwValue0 = (double)0.0;
		double dwValue1 = ( double )0.0;
		double dwValue2 = ( double )0.0;
		double dwValue3 = ( double )0.0;
		double dwValue4 = ( double )0.0;
		double dwValue5 = ( double )0.0;

		BOOL bContinue = FALSE;
		BOOL bDetected = FALSE;
	
		resize(matBlurImage, matTemp, Size(matBlurImage.cols / 2, matBlurImage.rows / 2 ));

		m_ptLeftArray.RemoveAll( );
		m_ptRightArray.RemoveAll( );

		Mat src,src_gray;
		src = matTemp;

		cvtColor( src, src_gray, COLOR_BGR2GRAY );
		src_gray = src_gray > 127;

		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;

		cv::Mat kernel = cv::Mat::ones(3, 1, CV_8U);
		erode(src_gray,src_gray, kernel, Point(-1,-1),2);
		findContours( src_gray, contours, hierarchy, RETR_LIST, CHAIN_APPROX_SIMPLE, Point(0, 0) );
			
		for( size_t i = 0 ; i < contours.size(); i++ )
		{
			Scalar color = Scalar( 0, 0, 255 );
			Rect R = boundingRect(Mat(contours[i]));

			Rect* d = new Rect;
			if  ( !( ( R.width < 6 ) || ( R.width > 22 ) ) ){
				if ( !( ( R.height < 6 ) || ( R.height > 24 ) ) ){
					//rectangle(src,R,color);
					if ( R.height > R.width ){
						//rectangle(src,R,color);

						Rect* d = new Rect;

						d->x = R.x;
						d->y = R.y;
						d->width = R.width;
						d->height = R.height;

						m_ptrArray1->Add( ( Rect* )d );
					}
				}
			}
		}

		k = 0;
		for( k = 0 ; k < m_ptrArray1->GetSize( ) ; k++ ) // 후보박스영역 모인곳
		{
			/*
			Rect* R = ( Rect* )m_ptrArray1->GetAt( k );
				
			Rect recta( R->x, R->y, R->width, R->height );
			Mat subImage = matTemp(recta);

			string str = format( "Neg\\%d.bmp", m_nIndex++ );
			imwrite( str.c_str(), subImage );
			*/

			Scalar color = Scalar( 0, 0, 255 );
			Rect* R = ( Rect* )m_ptrArray1->GetAt( k );
			rectangle(src,*R,color);
					
			Rect recta( R->x, R->y, R->width, R->height );
			Mat subImage = matTemp(recta);

			printf("c:\\Pos\\%d.jpg\n", nameNumbering++);
			///*
			string str = format( "c:\\Pos\\%d.jpg", nameNumbering++ );
			//string str = format( "d:\\Neg\\%d.jpgp", m_nIndex++ );
			imwrite( str.c_str(), subImage );
			//*/
		}

		imshow( "Exam", matTemp );
		string str = format( "Output.bmp" );
		//imwrite( str.c_str(), matTemp );
	}
}

void CNumberRecognizerDlg::OnBnClickedButtonDetectCharacter()
{
	DetectCharacterWithOpenImage();
}


void CNumberRecognizerDlg::OnBnClickedButtonTrainSVM()
{
	/*
	double dData[512];
	CLibSVMWrapper svm;	
	CLibSVMWrapper::_Node node;

	svm.SetDimensionCount(512);

	for( int i = 0; i < 100; i++ ) {
		// Feature를 추출하여 dData에 저장

		// dData를 node에 추가 후 SVM에 입력
		node.m_nLabel = 0;
		node.m_pValues = dData;
		svm.AddNode(node);	
	}
	svm.Train();
	svm.Save("weight.out");
	*/

	double dData[128];
	CLibSVMWrapper svm;	
	CLibSVMWrapper::_Node node;

	svm.SetDimensionCount(32);

	/*
	for( int i = 0; i < 206; i++ ) {
		// Feature를 추출하여 dData에 저장

		// dData를 node에 추가 후 SVM에 입력
		node.m_nLabel = 1;
		node.m_pValues = dData;
		svm.AddNode(node);	
	}
	*/


	CString tpath = "d:\\Pos\\*.*";

	//검색 클래스
	CFileFind finder;

	//CFileFind는 파일, 디렉터리가 존재하면 TRUE 를 반환함
	BOOL bWorking = finder.FindFile(tpath); //

	CString fileName;
	CString DirName;

	int i, j;
	int x, y;
	int nCount = 0;

	IplImage* Img;


	while (bWorking)
	{
			//다음 파일 / 폴더 가 존재하면다면 TRUE 반환
			bWorking = finder.FindNextFile();

		   //파일 일때
			if (finder.IsArchived())
			{
					//파일의 이름
				/*
					CString _fileName =  finder.GetFileName();
					_fileName.Format( "d:\\SampleImage.jpg" );
					CT2CA strAtl( _fileName );
					string str( strAtl );
					*/

				/*
					CString _fileName =  finder.GetFileName();
					CT2CA strAtl( _fileName );
					string str( strAtl );
					*/
				//CString previousImage = finder.GetFileName();
				CString previousImage = finder.GetFilePath();
				Img = cvLoadImage( previousImage );
					Mat	imgMat = cvarrToMat( Img );


					//Mat imgMat = imread( str );
					Mat imgResize;
					resize( imgMat, imgResize, Size( 8, 16 ), 0, 0, CV_INTER_NN );

					nCount = 0;
					for ( i = 0 ; i < 16 ; i++ ){
						for ( j = 0 ; j < 8 ; j++ ){
							dData[ nCount++ ] = ( imgResize.at<Vec3b>(i,j)[0] > 128 ) ? 1. : 0.;
						}
					}

					node.m_nLabel = 1;
					node.m_pValues = dData;
					svm.AddNode(node);	
			  }
	}



	tpath = "d:\\Neg\\*.*";

	//검색 클래스
	finder;

	//CFileFind는 파일, 디렉터리가 존재하면 TRUE 를 반환함
	bWorking = finder.FindFile(tpath); //

	while (bWorking)
	{
			//다음 파일 / 폴더 가 존재하면다면 TRUE 반환
			bWorking = finder.FindNextFile();

		   //파일 일때
			if (finder.IsArchived())
			{
				/*
					//파일의 이름
					CString _fileName =  finder.GetFileName();
					CT2CA strAtl( _fileName );
					string str( strAtl );

					Mat imgMat = imread( str );
					Mat imgResize;
					resize( imgMat, imgResize, Size( 8, 16 ), 0, 0, CV_INTER_NN );
					*/

				CString previousImage = finder.GetFilePath();
				Img = cvLoadImage( previousImage );
					Mat	imgMat = cvarrToMat( Img );

					//Mat imgMat = imread( str );
					Mat imgResize;
					resize( imgMat, imgResize, Size( 8, 16 ), 0, 0, CV_INTER_NN );

					nCount = 0;
					for ( i = 0 ; i < 16 ; i++ ){
						for ( j = 0 ; j < 8 ; j++ ){
							dData[ nCount++ ] = ( imgResize.at<Vec3b>(i,j)[0] > 128 ) ? 1. : 0.;
						}
					}

					node.m_nLabel = -1;
					node.m_pValues = dData;
					svm.AddNode(node);	

					/*
							node.m_nLabel = 1;
							node.m_pValues = dData;
							svm.AddNode(node);	
					*/
					//resize( imgMat, imgResize, Size( 4, 8 ), 0, 0, CV_INTER_NN );
					
					/*
					double dwSum = 0.0;
					for ( i = 0 ; i <= 1 ; i++ ){
						for ( j = 0 ; j <= 1 ; j++ ){
							if ( imgResize	
						}
					}

					i = 0;
					x = y = 0;
					while ( i < 16 ){
					}
					*/
			  }
	}


	svm.Train();
	svm.Save("MyWeight.out");

}

void MLP()
{
	CvTermCriteria term = cvTermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS ,5000, 0.001);
	
	Ptr<ml::ANN_MLP> mlp = ml::ANN_MLP::create();


}


void CNumberRecognizerDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: Add your message handler code here
	if ( m_ptrArray1 != NULL )
		m_ptrArray1->RemoveAll( );
}
